"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GameState_1 = require("./input/GameState");
var GameTickResult_1 = require("./data/GameTickResult");
var GameField_1 = require("./data/GameField");
var KeyObserver_1 = require("./input/KeyObserver");
var GameView_1 = require("./view/GameView");
var GameData_1 = require("./data/GameData");
var _game;
function onLoad() {
    var canvas = document.getElementById("canvas");
    if (canvas.getContext) {
        _game = new Game(canvas);
    }
    else {
        document.writeln("Error loading canvas or context.");
    }
}
var Game = (function () {
    function Game(canvas) {
        this.inputState = null;
        this.gameLoopInterval = null;
        this.gameData = null;
        this.maxPixelsEaten = 0;
        this.gameState = GameState_1.GameState.Initiating;
        this.pauseAllowed = false;
        this.keyObserver = new KeyObserver_1.KeyObserver();
        this.gameField = new GameField_1.GameField(canvas);
        this.gameView = new GameView_1.GameView(canvas);
        var self = this;
        setInterval(function () {
            self.onUserInput.apply(self);
        }, 1000 / 60);
        setInterval(function () {
            self.onRenderGame.apply(self);
        }, 1000 / 40);
    }
    Game.prototype.startGame = function () {
        this.gameData = new GameData_1.GameData(this.gameField);
        this.pauseAllowed = true;
        this.startGameLoop();
    };
    Game.prototype.startGameLoop = function () {
        var self = this;
        this.gameLoopInterval = setInterval(function () {
            self.onGameTick.apply(self);
        }, 1000 / 60);
    };
    Game.prototype.stopGameLoop = function () {
        clearInterval(this.gameLoopInterval);
    };
    Game.prototype.onUserInput = function () {
        this.inputState = this.keyObserver.getUserInputState();
        switch (this.gameState) {
            case GameState_1.GameState.Pause:
                if (this.inputState.p && this.pauseAllowed) {
                    this.gameState = GameState_1.GameState.Running;
                    this.startGameLoop();
                    this.forbidPause();
                }
                break;
            case GameState_1.GameState.Initiating:
                if (this.inputState.n) {
                    this.gameState = GameState_1.GameState.Running;
                    this.startGame();
                }
                break;
            case GameState_1.GameState.Running:
                if (this.inputState.p && this.pauseAllowed) {
                    this.gameState = GameState_1.GameState.Pause;
                    this.stopGameLoop();
                    this.forbidPause();
                }
                break;
            case GameState_1.GameState.GameOver:
                if (this.inputState.n) {
                    this.gameState = GameState_1.GameState.Running;
                    this.startGame();
                }
                break;
        }
    };
    Game.prototype.forbidPause = function () {
        this.pauseAllowed = false;
        var that = this;
        setTimeout(function () {
            that.pauseAllowed = true;
        }, 200);
    };
    Game.prototype.onGameTick = function () {
        var result = this.gameData.applyUserInput(this.inputState);
        if (result == GameTickResult_1.GameTickResult.GameOver) {
            this.stopGameLoop();
            this.gameState = GameState_1.GameState.GameOver;
        }
    };
    Game.prototype.onRenderGame = function () {
        this.gameView.clearCanvas();
        switch (this.gameState) {
            case GameState_1.GameState.Initiating:
                this.gameView.renderIntro();
                break;
            case GameState_1.GameState.Running:
                this.gameView.renderGame(this.gameData);
                break;
            case GameState_1.GameState.Pause:
                this.gameView.renderGame(this.gameData);
                this.gameView.renderPauseOverlay();
                break;
            case GameState_1.GameState.GameOver:
                this.maxPixelsEaten = Math.max(this.maxPixelsEaten, this.gameData.player.pixelsEaten);
                this.gameView.renderGame(this.gameData);
                this.gameView.renderGameEnd(this.gameData.player, this.maxPixelsEaten);
                break;
        }
        this.gameView.renderInputState(this.inputState);
    };
    return Game;
}());
exports.Game = Game;
