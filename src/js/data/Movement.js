"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Common_1 = require("../Common");
var Movement = (function () {
    function Movement(maxSpeed) {
        this.speedX = 0;
        this.speedY = 0;
        this.acceleration = 3;
        this.deceleration = 1;
        this.maxSpeed = maxSpeed;
    }
    Movement.prototype.getSpeedX = function () {
        return this.speedX;
    };
    Movement.prototype.getSpeedY = function () {
        return this.speedY;
    };
    Movement.prototype.updateForInput = function (inputState) {
        this.speedX = this.updateSpeed(inputState.right, inputState.left, this.speedX);
        this.speedY = this.updateSpeed(inputState.down, inputState.up, this.speedY);
        this.limitSpeed();
    };
    Movement.prototype.updateRandom = function () {
        this.speedX += Common_1.getRandom(-1, 1);
        this.speedY += Common_1.getRandom(-1, 1);
        this.limitSpeed();
    };
    Movement.prototype.limitSpeed = function () {
        if (this.speedX > this.maxSpeed) {
            this.speedX = this.maxSpeed;
        }
        else if (this.speedX < 0 - this.maxSpeed) {
            this.speedX = 0 - this.maxSpeed;
        }
        if (this.speedY > this.maxSpeed) {
            this.speedY = this.maxSpeed;
        }
        else if (this.speedY < 0 - this.maxSpeed) {
            this.speedY = 0 - this.maxSpeed;
        }
    };
    Movement.prototype.updateSpeed = function (increase, decrease, speed) {
        var newSpeed = speed;
        if (increase) {
            newSpeed = speed + this.acceleration;
        }
        else if (decrease) {
            newSpeed = speed - this.acceleration;
        }
        else if (speed != 0) {
            newSpeed = (speed < 0)
                ? speed + this.deceleration
                : speed - this.deceleration;
        }
        return newSpeed;
    };
    return Movement;
}());
exports.Movement = Movement;
