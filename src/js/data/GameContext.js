"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GameObject_1 = require("./GameObject");
var GameContext = (function () {
    function GameContext(gameField, gameData) {
        this.gameField = gameField;
        this.gameData = gameData;
    }
    GameContext.prototype.getSizeFactor = function () {
        var standard = 800 * 600;
        var current = this.getMaxX() * this.getMaxY();
        var factor = 1;
        if (current > standard) {
            factor = current / standard;
        }
        return factor;
    };
    GameContext.prototype.getPlayer = function () {
        return this.gameData.player;
    };
    GameContext.prototype.getNearPlayerArea = function () {
        var margin = 200;
        var nearPlayer = new GameObject_1.GameObject(this);
        nearPlayer.size = this.getPlayer().size + margin * 2;
        nearPlayer.x = this.getPlayer().x - margin;
        nearPlayer.y = this.getPlayer().y - margin;
        return nearPlayer;
    };
    GameContext.prototype.getMaxX = function () {
        return this.gameField.getWidth();
    };
    GameContext.prototype.getMaxY = function () {
        return this.gameField.getHeight();
    };
    GameContext.prototype.getMaxEnemySize = function () {
        return Math.round(this.getPlayer().size * 1.5);
    };
    return GameContext;
}());
exports.GameContext = GameContext;
