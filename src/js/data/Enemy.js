"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Common_1 = require("../Common");
var GameObject_1 = require("./GameObject");
var Movement_1 = require("./Movement");
var Enemy = (function (_super) {
    __extends(Enemy, _super);
    function Enemy(gameContext) {
        var _this = _super.call(this, gameContext) || this;
        _this.minSize = 10;
        _this.maxSize = 150;
        _this.color = "rgb(255,0,0)";
        _this.movement = new Movement_1.Movement(8);
        _this.initialX = 0 - _this.maxSize * 2;
        _this.initialY = 0 - _this.maxSize * 2;
        _this.x = _this.initialX;
        _this.y = _this.initialY;
        // start with too many ticks outside game area
        // so with the next game tick the enemy is reset
        _this.maxTicksOutsideOfGameArea = 200;
        _this.ticksOutsideOfGameArea = _this.maxTicksOutsideOfGameArea + 1;
        return _this;
    }
    Enemy.prototype.onGameTick = function () {
        // reset deserter
        if (this.isWithinGameArea()) {
            this.ticksOutsideOfGameArea = 0;
        }
        else {
            this.ticksOutsideOfGameArea++;
            if (this.ticksOutsideOfGameArea > this.maxTicksOutsideOfGameArea) {
                this.reset();
            }
        }
        this.movement.updateRandom();
        this.move();
        this.updateColor();
    };
    Enemy.prototype.updateColor = function () {
        var playerPixels = this.gameContext.getPlayer().getPixelCount();
        this.color = (playerPixels >= this.getPixelCount())
            ? "rgb(0,255,0)"
            : "rgb(255,0,0)";
    };
    Enemy.prototype.reset = function () {
        this.size = Common_1.getRandom(this.minSize, this.gameContext.getMaxEnemySize());
        this.wantSize = null;
        var nearPlayer = this.gameContext.getNearPlayerArea();
        do {
            var minX = 0 - this.size - this.maxOffset;
            var maxX = this.gameContext.gameField.getWidth() + this.maxOffset;
            this.x = Common_1.getRandom(minX, maxX);
            var minY = 0 - this.size - this.maxOffset;
            var maxY = this.gameContext.gameField.getHeight() + this.maxOffset;
            this.y = Common_1.getRandom(minY, maxY);
        } while (this.intersectsWith(nearPlayer) || this.isWithinGameArea());
    };
    return Enemy;
}(GameObject_1.GameObject));
exports.Enemy = Enemy;
