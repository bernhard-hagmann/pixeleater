export enum GameTickResult {
    GameOver,
    Running,
    ZoomOut
}
