"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GameObject = (function () {
    function GameObject(gameContext) {
        this.x = null;
        this.y = null;
        this.color = null;
        this.size = null;
        this.minSize = null;
        this.maxSize = null;
        this.movement = null;
        this.wantSize = null;
        this.growing = false;
        this.shrinking = false;
        this.gameContext = gameContext;
    }
    GameObject.prototype.getPixelCount = function () {
        return Math.pow(this.size, 2);
    };
    GameObject.prototype.move = function () {
        var sizingFactor = 0.10;
        if (this.wantSize != null) {
            var sizeDiff = Math.round(this.size * sizingFactor);
            if (this.shrinking && this.size > this.wantSize) {
                this.size = this.size - sizeDiff;
            }
            else if (this.growing && this.size < this.wantSize) {
                this.size = this.size + sizeDiff;
            }
            else {
                this.wantSize = null;
                this.growing = false;
                this.shrinking = false;
            }
        }
        this.x += this.movement.getSpeedX();
        this.y += this.movement.getSpeedY();
    };
    GameObject.prototype.resize = function (percent) {
        this.growing = percent > 100;
        this.shrinking = percent < 100;
        this.wantSize = Math.round(this.size / 100 * percent);
        // ensure size limits
        this.wantSize = Math.max(this.wantSize, this.minSize);
        this.wantSize = Math.min(this.wantSize, this.maxSize);
    };
    GameObject.prototype.getRect = function () {
        return {
            left: this.x,
            top: this.y,
            right: this.x + this.size,
            bottom: this.y + this.size
        };
    };
    GameObject.prototype.intersectsWith = function (other) {
        var r1 = this.getRect();
        var r2 = other.getRect();
        return !(r2.left > r1.right
            || r2.right < r1.left
            || r2.top > r1.bottom
            || r2.bottom < r1.top);
    };
    GameObject.prototype.isBiggerOrEqual = function (other) {
        return this.size >= other.size;
    };
    GameObject.prototype.isWithinGameArea = function () {
        return (this.x + this.size) > 0
            && this.x < this.gameContext.getMaxX()
            && (this.y + this.size) > 0
            && this.y < this.gameContext.getMaxY();
    };
    return GameObject;
}());
exports.GameObject = GameObject;
