"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Enemy_1 = require("./Enemy");
var GameContext_1 = require("./GameContext");
var GameTickResult_1 = require("./GameTickResult");
var Player_1 = require("./Player");
var GameData = (function () {
    function GameData(gameField) {
        this.zoomFactor = 1;
        var gameContext = new GameContext_1.GameContext(gameField, this);
        this.player = new Player_1.Player(gameContext);
        this.enemyCount = 40 * gameContext.getSizeFactor();
        this.enemies = [];
        for (var i = 0; i < this.enemyCount; i++) {
            this.enemies.push(new Enemy_1.Enemy(gameContext));
        }
    }
    GameData.prototype.applyUserInput = function (inputState) {
        var result = GameTickResult_1.GameTickResult.Running;
        if (this.player.size >= this.player.maxSize) {
            // zoom out
            this.zoomFactor *= 2;
            this.player.resize(0.5);
            for (var i = 0; i < this.enemies.length; i++) {
                this.enemies[i].resize(0.5);
            }
            result = GameTickResult_1.GameTickResult.ZoomOut;
        }
        this.player.onGameTick(inputState);
        for (var i = 0; i < this.enemies.length; i++) {
            var enemy = this.enemies[i];
            enemy.onGameTick();
            if (enemy.intersectsWith(this.player)) {
                // player > enemy  -->  eat
                if (this.player.isBiggerOrEqual(enemy)) {
                    this.player.eat(enemy, this.zoomFactor);
                    enemy.reset();
                }
                else {
                    result = GameTickResult_1.GameTickResult.GameOver;
                }
            }
        }
        return result;
    };
    return GameData;
}());
exports.GameData = GameData;
