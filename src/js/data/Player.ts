import {GameObject} from "./GameObject";
import {Movement} from "./Movement";

export class Player extends GameObject {

    green: number = 0;
    blueToGreen: boolean = true;
    colorChangeStep: number = 30;
    pixelsEaten: number = 0;

    constructor(gameContext) {
        super(gameContext);

        this.minSize = 20;
        this.maxSize = 150;
        this.size = this.minSize;
        this.movement = new Movement(15);

        const midX = this.gameContext.getMaxX() / 2;
        const midY = this.gameContext.getMaxY() / 2;
        this.x = Math.round(midX - this.size / 2);
        this.y = Math.round(midY - this.size / 2);
        this.color = "rgb(0,0,255)";
    }

    onGameTick(inputState) {
        this.pulseColor();
        this.movement.updateForInput(inputState);
        this.move();
        this.ensurePositionWithinGameArea();
    }

    ensurePositionWithinGameArea() {
        // check right
        const maxX = this.gameContext.getMaxX();
        if (this.getRect().right > maxX) {
            this.x = maxX - this.size;
        }

        // check left
        this.x = Math.max(this.x, 0);

        // check bottom
        const maxY = this.gameContext.getMaxY();
        if (this.getRect().bottom > maxY) {
            this.y = maxY - this.size;
        }

        // check top
        this.y = Math.max(this.y, 0);
    }

    pulseColor() {
        // reverse color-change if the limit has been reached
        if (this.blueToGreen && this.green >= 255
            || !this.blueToGreen && this.green <= 0) {
            this.blueToGreen = !this.blueToGreen;
        }

        if (this.blueToGreen) {
            this.green += this.colorChangeStep;
        }
        else {
            this.green -= this.colorChangeStep;
        }

        this.color = "rgb(0," + this.green + ",255)";
    }

    eat(enemy, zoomFactor) {
        // update eaten pixels
        const enemyPixels = enemy.getPixelCount();
        const playerPixels = this.getPixelCount();
        this.pixelsEaten += enemyPixels * zoomFactor;

        // increase size to fit enemy into belly
        const enemyInPercentOfPlayer = Math.round(enemyPixels * 100 / playerPixels);
        this.resize(100 + enemyInPercentOfPlayer);
    }

}