"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GameTickResult;
(function (GameTickResult) {
    GameTickResult[GameTickResult["GameOver"] = 0] = "GameOver";
    GameTickResult[GameTickResult["Running"] = 1] = "Running";
    GameTickResult[GameTickResult["ZoomOut"] = 2] = "ZoomOut";
})(GameTickResult = exports.GameTickResult || (exports.GameTickResult = {}));
