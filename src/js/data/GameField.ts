export class GameField {

    canvas: any;

    constructor(canvas) {
        this.canvas = canvas;
    }

    getHeight() {
        return this.canvas.height;
    }

    getWidth() {
        return this.canvas.width;
    }

}
