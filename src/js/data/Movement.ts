import {getRandom} from "../Common";

export class Movement {

    maxSpeed: number;
    speedX: number = 0;
    speedY: number = 0;
    acceleration: number = 3;
    deceleration: number = 1;

    constructor(maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    getSpeedX() {
        return this.speedX;
    }

    getSpeedY() {
        return this.speedY;
    }

    updateForInput(inputState) {
        this.speedX = this.updateSpeed(inputState.right, inputState.left, this.speedX);
        this.speedY = this.updateSpeed(inputState.down, inputState.up, this.speedY);
        this.limitSpeed();
    }

    updateRandom() {
        this.speedX += getRandom(-1, 1);
        this.speedY += getRandom(-1, 1);
        this.limitSpeed();
    }

    limitSpeed() {
        if (this.speedX > this.maxSpeed) {
            this.speedX = this.maxSpeed;
        } else if (this.speedX < 0 - this.maxSpeed) {
            this.speedX = 0 - this.maxSpeed;
        }

        if (this.speedY > this.maxSpeed) {
            this.speedY = this.maxSpeed;
        } else if (this.speedY < 0 - this.maxSpeed) {
            this.speedY = 0 - this.maxSpeed;
        }
    }

    updateSpeed(increase, decrease, speed) {
        let newSpeed = speed;

        if (increase) {
            newSpeed = speed + this.acceleration;
        }
        else if (decrease) {
            newSpeed = speed - this.acceleration;
        } else if (speed != 0) {
            newSpeed = (speed < 0)
                ? speed + this.deceleration
                : speed - this.deceleration;
        }

        return newSpeed;
    }

}
