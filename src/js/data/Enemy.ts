import {getRandom} from "../Common";
import {GameObject} from "./GameObject";
import {Movement} from "./Movement";
import {GameContext} from "./GameContext";

export class Enemy extends GameObject {

    minSize: number = 10;
    maxSize: number = 150;
    color: string = "rgb(255,0,0)";
    movement: Movement = new Movement(8);
    initialX: number = 0 - this.maxSize * 2;
    initialY: number = 0 - this.maxSize * 2;
    x: number = this.initialX;
    y: number = this.initialY;
    maxOffset: number;
    // start with too many ticks outside game area
    // so with the next game tick the enemy is reset
    maxTicksOutsideOfGameArea: number = 200;
    ticksOutsideOfGameArea: number = this.maxTicksOutsideOfGameArea + 1;

    constructor(gameContext: GameContext) {
        super(gameContext);
    }


    onGameTick() {
        // reset deserter
        if (this.isWithinGameArea()) {
            this.ticksOutsideOfGameArea = 0;
        } else {
            this.ticksOutsideOfGameArea++;
            if (this.ticksOutsideOfGameArea > this.maxTicksOutsideOfGameArea) {
                this.reset();
            }
        }

        this.movement.updateRandom();
        this.move();
        this.updateColor();
    }

    updateColor() {
        const playerPixels = this.gameContext.getPlayer().getPixelCount();
        this.color = (playerPixels >= this.getPixelCount())
            ? "rgb(0,255,0)"
            : "rgb(255,0,0)";
    }

    reset() {
        this.size = getRandom(this.minSize, this.gameContext.getMaxEnemySize());
        this.wantSize = null;

        const nearPlayer = this.gameContext.getNearPlayerArea();
        do {
            const minX = 0 - this.size - this.maxOffset;
            const maxX = this.gameContext.gameField.getWidth() + this.maxOffset;
            this.x = getRandom(minX, maxX);

            const minY = 0 - this.size - this.maxOffset;
            const maxY = this.gameContext.gameField.getHeight() + this.maxOffset;
            this.y = getRandom(minY, maxY);
        } while (this.intersectsWith(nearPlayer) || this.isWithinGameArea());
    }
}
