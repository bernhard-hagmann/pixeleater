"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var GameObject_1 = require("./GameObject");
var Movement_1 = require("./Movement");
var Player = (function (_super) {
    __extends(Player, _super);
    function Player(gameContext) {
        var _this = _super.call(this, gameContext) || this;
        _this.green = 0;
        _this.blueToGreen = true;
        _this.colorChangeStep = 30;
        _this.pixelsEaten = 0;
        _this.minSize = 20;
        _this.maxSize = 150;
        _this.size = _this.minSize;
        _this.movement = new Movement_1.Movement(15);
        var midX = _this.gameContext.getMaxX() / 2;
        var midY = _this.gameContext.getMaxY() / 2;
        _this.x = Math.round(midX - _this.size / 2);
        _this.y = Math.round(midY - _this.size / 2);
        _this.color = "rgb(0,0,255)";
        return _this;
    }
    Player.prototype.onGameTick = function (inputState) {
        this.pulseColor();
        this.movement.updateForInput(inputState);
        this.move();
        this.ensurePositionWithinGameArea();
    };
    Player.prototype.ensurePositionWithinGameArea = function () {
        // check right
        var maxX = this.gameContext.getMaxX();
        if (this.getRect().right > maxX) {
            this.x = maxX - this.size;
        }
        // check left
        this.x = Math.max(this.x, 0);
        // check bottom
        var maxY = this.gameContext.getMaxY();
        if (this.getRect().bottom > maxY) {
            this.y = maxY - this.size;
        }
        // check top
        this.y = Math.max(this.y, 0);
    };
    Player.prototype.pulseColor = function () {
        // reverse color-change if the limit has been reached
        if (this.blueToGreen && this.green >= 255
            || !this.blueToGreen && this.green <= 0) {
            this.blueToGreen = !this.blueToGreen;
        }
        if (this.blueToGreen) {
            this.green += this.colorChangeStep;
        }
        else {
            this.green -= this.colorChangeStep;
        }
        this.color = "rgb(0," + this.green + ",255)";
    };
    Player.prototype.eat = function (enemy, zoomFactor) {
        // update eaten pixels
        var enemyPixels = enemy.getPixelCount();
        var playerPixels = this.getPixelCount();
        this.pixelsEaten += enemyPixels * zoomFactor;
        // increase size to fit enemy into belly
        var enemyInPercentOfPlayer = Math.round(enemyPixels * 100 / playerPixels);
        this.resize(100 + enemyInPercentOfPlayer);
    };
    return Player;
}(GameObject_1.GameObject));
exports.Player = Player;
