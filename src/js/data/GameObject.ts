import {GameContext} from "./GameContext";

export class GameObject {

    x: number = null;
    y: number = null;
    color: string = null;
    size: number = null;
    minSize: number = null;
    maxSize: number = null;
    movement: Movement = null;
    wantSize: number = null;
    growing: boolean = false;
    shrinking: boolean = false;
    gameContext: GameContext;

    constructor(gameContext: GameContext) {
        this.gameContext = gameContext;
    }

    getPixelCount() {
        return Math.pow(this.size, 2);
    }

    move() {
        const sizingFactor = 0.10;
        if (this.wantSize != null) {
            const sizeDiff = Math.round(this.size * sizingFactor);
            if (this.shrinking && this.size > this.wantSize) {
                this.size = this.size - sizeDiff;
            } else if (this.growing && this.size < this.wantSize) {
                this.size = this.size + sizeDiff;
            } else {
                this.wantSize = null;
                this.growing = false;
                this.shrinking = false;
            }
        }
        this.x += this.movement.getSpeedX();
        this.y += this.movement.getSpeedY();
    }

    resize(percent) {
        this.growing = percent > 100;
        this.shrinking = percent < 100;
        this.wantSize = Math.round(this.size / 100 * percent);

        // ensure size limits
        this.wantSize = Math.max(this.wantSize, this.minSize);
        this.wantSize = Math.min(this.wantSize, this.maxSize);
    }

    getRect() {
        return {
            left: this.x,
            top: this.y,
            right: this.x + this.size,
            bottom: this.y + this.size
        };
    }

    intersectsWith(other) {
        const r1 = this.getRect();
        const r2 = other.getRect();

        return !(
            r2.left > r1.right
            || r2.right < r1.left
            || r2.top > r1.bottom
            || r2.bottom < r1.top
        );
    }

    isBiggerOrEqual(other) {
        return this.size >= other.size;
    }

    isWithinGameArea() {
        return (this.x + this.size) > 0
            && this.x < this.gameContext.getMaxX()
            && (this.y + this.size) > 0
            && this.y < this.gameContext.getMaxY();
    }
}