import {Enemy} from "./Enemy";
import {GameContext} from "./GameContext";
import {GameTickResult} from "./GameTickResult";
import {Player} from "./Player";

export class GameData {

    zoomFactor: number = 1;
    player: Player;
    enemyCount: number;
    enemies: Enemy[];

    constructor(gameField) {
        const gameContext = new GameContext(gameField, this);
        this.player = new Player(gameContext);

        this.enemyCount = 40 * gameContext.getSizeFactor();
        this.enemies = [];
        for (let i = 0; i < this.enemyCount; i++) {
            this.enemies.push(new Enemy(gameContext));
        }
    }

    applyUserInput(inputState) {
        let result = GameTickResult.Running;

        if (this.player.size >= this.player.maxSize) {
            // zoom out
            this.zoomFactor *= 2;
            this.player.resize(0.5);
            for (let i = 0; i < this.enemies.length; i++) {
                this.enemies[i].resize(0.5);
            }
            result = GameTickResult.ZoomOut;
        }

        this.player.onGameTick(inputState);

        for (let i = 0; i < this.enemies.length; i++) {
            const enemy = this.enemies[i];
            enemy.onGameTick();

            if (enemy.intersectsWith(this.player)) {
                // player > enemy  -->  eat
                if (this.player.isBiggerOrEqual(enemy)) {
                    this.player.eat(enemy, this.zoomFactor);
                    enemy.reset();
                }
                // player < enemy  -->  game over
                else {
                    result = GameTickResult.GameOver;
                }
            }
        }

        return result;
    }

}
