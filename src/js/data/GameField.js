"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GameField = (function () {
    function GameField(canvas) {
        this.canvas = canvas;
    }
    GameField.prototype.getHeight = function () {
        return this.canvas.height;
    };
    GameField.prototype.getWidth = function () {
        return this.canvas.width;
    };
    return GameField;
}());
exports.GameField = GameField;
