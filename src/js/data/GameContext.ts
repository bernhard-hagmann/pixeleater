import {GameField} from "./GameField";
import {GameData} from "./GameData";
import {GameObject} from "./GameObject";

export class GameContext {

    gameField: GameField;
    gameData: GameData;

    constructor(gameField: GameField, gameData: GameData) {
        this.gameField = gameField;
        this.gameData = gameData;
    }

    getSizeFactor() {
        const standard = 800 * 600;
        const current = this.getMaxX() * this.getMaxY();

        let factor = 1;
        if (current > standard) {
            factor = current / standard;
        }

        return factor;
    }

    getPlayer() {
        return this.gameData.player;
    }

    getNearPlayerArea() {
        const margin = 200;
        const nearPlayer = new GameObject(this);
        nearPlayer.size = this.getPlayer().size + margin * 2;
        nearPlayer.x = this.getPlayer().x - margin;
        nearPlayer.y = this.getPlayer().y - margin;
        return nearPlayer;
    }

    getMaxX() {
        return this.gameField.getWidth();
    }

    getMaxY() {
        return this.gameField.getHeight();
    }

    getMaxEnemySize() {
        return Math.round(this.getPlayer().size * 1.5);
    }

}
