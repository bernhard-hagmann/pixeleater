import {KeyCode} from "./KeyCodes";

export class KeyObserver {

    keys = {};

    constructor() {
        window.addEventListener("keydown", (e) => this.onKeyDown(e, this), true);
        window.addEventListener("keyup", (e) => this.onKeyUp(e, this), true);
    }

    getUserInputState() {
        return {
            up: this.isPressed(KeyCode.Up) || this.isPressed(KeyCode.w),
            down: this.isPressed(KeyCode.Down) || this.isPressed(KeyCode.s),
            left: this.isPressed(KeyCode.Left) || this.isPressed(KeyCode.a),
            right: this.isPressed(KeyCode.Right) || this.isPressed(KeyCode.d),
            n: this.isPressed(KeyCode.n),
            p: this.isPressed(KeyCode.p)
        };
    }

    isPressed(code) {
        return this.keys.hasOwnProperty(code) && this.keys[code];
    }

    onKeyDown(eventArgs, self) {
        self.setKey(eventArgs.keyCode, true);
    }

    onKeyUp(eventArgs, self) {
        self.setKey(eventArgs.keyCode, false);
    }

    setKey(code, pressed) {
        this.keys[code] = pressed;
    }

}
