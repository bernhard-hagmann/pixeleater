"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GameState;
(function (GameState) {
    GameState[GameState["Initiating"] = 0] = "Initiating";
    GameState[GameState["Running"] = 1] = "Running";
    GameState[GameState["GameOver"] = 2] = "GameOver";
    GameState[GameState["Pause"] = 3] = "Pause";
})(GameState = exports.GameState || (exports.GameState = {}));
