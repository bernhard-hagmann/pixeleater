export enum GameState {
    Initiating,
    Running,
    GameOver,
    Pause
}
