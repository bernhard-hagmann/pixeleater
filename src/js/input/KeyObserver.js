"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var KeyCodes_1 = require("./KeyCodes");
var KeyObserver = (function () {
    function KeyObserver() {
        var _this = this;
        this.keys = {};
        window.addEventListener("keydown", function (e) { return _this.onKeyDown(e, _this); }, true);
        window.addEventListener("keyup", function (e) { return _this.onKeyUp(e, _this); }, true);
    }
    KeyObserver.prototype.getUserInputState = function () {
        return {
            up: this.isPressed(KeyCodes_1.KeyCode.Up) || this.isPressed(KeyCodes_1.KeyCode.w),
            down: this.isPressed(KeyCodes_1.KeyCode.Down) || this.isPressed(KeyCodes_1.KeyCode.s),
            left: this.isPressed(KeyCodes_1.KeyCode.Left) || this.isPressed(KeyCodes_1.KeyCode.a),
            right: this.isPressed(KeyCodes_1.KeyCode.Right) || this.isPressed(KeyCodes_1.KeyCode.d),
            n: this.isPressed(KeyCodes_1.KeyCode.n),
            p: this.isPressed(KeyCodes_1.KeyCode.p)
        };
    };
    KeyObserver.prototype.isPressed = function (code) {
        return this.keys.hasOwnProperty(code) && this.keys[code];
    };
    KeyObserver.prototype.onKeyDown = function (eventArgs, self) {
        self.setKey(eventArgs.keyCode, true);
    };
    KeyObserver.prototype.onKeyUp = function (eventArgs, self) {
        self.setKey(eventArgs.keyCode, false);
    };
    KeyObserver.prototype.setKey = function (code, pressed) {
        this.keys[code] = pressed;
    };
    return KeyObserver;
}());
exports.KeyObserver = KeyObserver;
