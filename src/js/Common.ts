export function getNumberString(number) {
    let str = Math.floor(number) + '';
    const regEx = /(\d+)(\d{3})/;
    while (regEx.test(str)) {
        str = str.replace(regEx, '$1,$2');
    }
    return str;
}

export function getRandom(min, max) {
    const canBeNegative = min < 0;
    const isNegative = canBeNegative && Math.random() < 0.5;
    let result;
    if (isNegative) {
        result = 0 - Math.floor(Math.random() * min + 1);
    }
    else {
        result = Math.floor(Math.random() * (max - min + 1) + min);
    }
    return result;
}
