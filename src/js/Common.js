"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getNumberString(number) {
    var str = Math.floor(number) + '';
    var regEx = /(\d+)(\d{3})/;
    while (regEx.test(str)) {
        str = str.replace(regEx, '$1,$2');
    }
    return str;
}
exports.getNumberString = getNumberString;
function getRandom(min, max) {
    var canBeNegative = min < 0;
    var isNegative = canBeNegative && Math.random() < 0.5;
    var result;
    if (isNegative) {
        result = 0 - Math.floor(Math.random() * min + 1);
    }
    else {
        result = Math.floor(Math.random() * (max - min + 1) + min);
    }
    return result;
}
exports.getRandom = getRandom;
