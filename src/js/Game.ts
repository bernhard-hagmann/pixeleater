﻿import {GameState} from "./input/GameState";
import {GameTickResult} from "./data/GameTickResult";
import {GameField} from "./data/GameField";
import {KeyObserver} from "./input/KeyObserver";
import {GameView} from "./view/GameView";
import {GameData} from "./data/GameData";

let _game;

export function onLoad() {
    const canvas: any = document.getElementById("canvas");
    if (canvas.getContext) {
        _game = new Game(canvas);
    } else {
        document.writeln("Error loading canvas or context.");
    }
}

export class Game {

    inputState = null;
    gameLoopInterval = null;
    gameData = null;
    maxPixelsEaten = 0;
    gameState = GameState.Initiating;
    pauseAllowed = false;
    gameField: GameField;
    keyObserver: KeyObserver = new KeyObserver();
    gameView: GameView;

    constructor(canvas: any) {
        this.gameField = new GameField(canvas);
        this.gameView = new GameView(canvas);

        const self = this;
        setInterval(
            function () {
                self.onUserInput.apply(self);
            },
            1000 / 60);
        setInterval(
            function () {
                self.onRenderGame.apply(self);
            },
            1000 / 40);
    }

    startGame() {
        this.gameData = new GameData(this.gameField);
        this.pauseAllowed = true;
        this.startGameLoop();
    }

    startGameLoop() {
        const self = this;
        this.gameLoopInterval = setInterval(
            function () {
                self.onGameTick.apply(self);
            },
            1000 / 60);
    }

    stopGameLoop() {
        clearInterval(this.gameLoopInterval);
    }

    onUserInput() {
        this.inputState = this.keyObserver.getUserInputState();

        switch (this.gameState) {
            case GameState.Pause:
                if (this.inputState.p && this.pauseAllowed) {
                    this.gameState = GameState.Running;
                    this.startGameLoop();
                    this.forbidPause();
                }
                break;

            case GameState.Initiating:
                if (this.inputState.n) {
                    this.gameState = GameState.Running;
                    this.startGame();
                }
                break;

            case GameState.Running:
                if (this.inputState.p && this.pauseAllowed) {
                    this.gameState = GameState.Pause;
                    this.stopGameLoop();
                    this.forbidPause();
                }
                break;

            case GameState.GameOver:
                if (this.inputState.n) {
                    this.gameState = GameState.Running;
                    this.startGame();
                }
                break;
        }
    }

    forbidPause() {
        this.pauseAllowed = false;
        const that = this;
        setTimeout(function () {
            that.pauseAllowed = true;
        }, 200);
    }

    onGameTick() {
        const result = this.gameData.applyUserInput(this.inputState);
        if (result == GameTickResult.GameOver) {
            this.stopGameLoop();
            this.gameState = GameState.GameOver;
        }
    }

    onRenderGame() {
        this.gameView.clearCanvas();

        switch (this.gameState) {
            case GameState.Initiating:
                this.gameView.renderIntro();
                break;

            case GameState.Running:
                this.gameView.renderGame(this.gameData);
                break;

            case GameState.Pause:
                this.gameView.renderGame(this.gameData);
                this.gameView.renderPauseOverlay();
                break;

            case GameState.GameOver:
                this.maxPixelsEaten = Math.max(this.maxPixelsEaten, this.gameData.player.pixelsEaten);

                this.gameView.renderGame(this.gameData);
                this.gameView.renderGameEnd(this.gameData.player, this.maxPixelsEaten);
                break;
        }

        this.gameView.renderInputState(this.inputState);
    }
}