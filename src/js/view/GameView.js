"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Common_1 = require("../Common");
var GameView = (function () {
    function GameView(canvas) {
        this.fontSet = "georgia, 'times new roman', serif";
        this.monoSpaceFontSet = "'courier new', courier, arial, sans-serif";
        this.canvas = canvas;
        this.context = this.canvas.getContext("2d");
        this.context.strokeStyle = "#fff";
    }
    GameView.prototype.renderIntro = function () {
        var logo = this.loadImage("logo.png");
        this.context.drawImage(logo, this.getMidX() - 300, this.getMidY() - 200);
        this.context.textAlign = "left";
        this.context.font = "4em " + this.fontSet;
        this.context.fillStyle = "black";
        this.printText("Pixel Eater", this.getMidX() - 50, this.getMidY() - 100);
        this.context.font = "1em " + this.fontSet;
        this.printText("v1.6", this.getMidX() + 200, this.getMidY() - 75);
        this.context.font = "1em " + this.fontSet;
        var x = this.getMidX() - 300;
        this.printText("Become the biggest pixel-mob ever to rule the world (well, at least the canvas)!", x, this.getMidY());
        this.printText("Use the arrow keys to move your blue pulsing pixels.", x, this.getMidY() + 30);
        this.printText("Eat smaller pixel-mobs by tackling them and avoid bigger pixel-aggregations.", x, this.getMidY() + 60);
        this.renderNewGameKeyInfo(this.getMidX() - 250, this.getMidY() + 130);
    };
    GameView.prototype.renderGameEnd = function (player, maxPixelsEaten) {
        this.context.textAlign = "center";
        this.context.font = "3em " + this.fontSet;
        this.context.fillStyle = "red";
        this.printText("Game over!", this.getMidX(), this.getMidY() - 30);
        this.context.fillStyle = "black";
        this.printText("Pixels eaten: " + Common_1.getNumberString(player.pixelsEaten), this.getMidX(), this.getMidY() + 30);
        this.context.font = "1em " + this.fontSet;
        this.printText("Pixels eaten (best): " + Common_1.getNumberString(maxPixelsEaten), this.getMidX(), this.getMidY() + 50);
        this.renderNewGameKeyInfo(this.getMidX(), this.getMidY() + 120);
    };
    GameView.prototype.renderGame = function (gameData) {
        for (var i = 0; i < gameData.enemies.length; i++) {
            var enemy = gameData.enemies[i];
            this.renderObject(enemy);
        }
        this.renderObject(gameData.player);
        this.renderGameInfo(gameData);
    };
    GameView.prototype.renderObject = function (obj) {
        this.context.fillStyle = obj.color;
        this.context.fillRect(obj.x, obj.y, obj.size, obj.size);
        this.context.strokeRect(obj.x - 1, obj.y - 1, obj.size + 1, obj.size + 1);
    };
    GameView.prototype.renderGameInfo = function (gameData) {
        this.context.textAlign = "left";
        this.context.font = "13px " + this.monoSpaceFontSet;
        this.context.fillStyle = "black";
        this.printText("Zoom factor: " + gameData.zoomFactor, 1, this.getCanvasTextBottom() - 14);
        var pixelsEaten = Common_1.getNumberString(gameData.player.pixelsEaten);
        this.printText("Pixels eaten: " + pixelsEaten, 1, this.getCanvasTextBottom());
    };
    GameView.prototype.renderPauseOverlay = function () {
        this.context.textAlign = "center";
        this.context.font = "80px " + this.fontSet;
        this.printText("PAUSE", this.getMidX(), this.getMidY());
    };
    GameView.prototype.renderInputState = function (inputState) {
        this.context.textAlign = "right";
        this.context.font = "13px " + this.monoSpaceFontSet;
        var stateString = "";
        if (inputState.left) {
            stateString += " < ";
        }
        if (inputState.up) {
            stateString += " ^ ";
        }
        if (inputState.down) {
            stateString += " v ";
        }
        if (inputState.right) {
            stateString += " > ";
        }
        if (inputState.n) {
            stateString += " n ";
        }
        if (inputState.p) {
            stateString += " p ";
        }
        this.printText(stateString, this.canvas.width - 1, this.getCanvasTextBottom());
    };
    GameView.prototype.getCanvasTextBottom = function () {
        return this.canvas.height - 3;
    };
    GameView.prototype.renderNewGameKeyInfo = function (x, y) {
        this.context.fillStyle = "black";
        this.context.font = "2em " + this.fontSet;
        this.printText("Press 'N' to start a new game.", x, y);
    };
    GameView.prototype.clearCanvas = function () {
        this.context.fillStyle = "rgb(255,255,255)";
        this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
    };
    GameView.prototype.loadImage = function (name) {
        var img = new Image();
        img.src = "img/" + name;
        return img;
    };
    GameView.prototype.getMidX = function () {
        return this.canvas.width / 2;
    };
    GameView.prototype.getMidY = function () {
        return this.canvas.height / 2;
    };
    GameView.prototype.printText = function (text, x, y) {
        this.context.strokeText(text, x, y);
        this.context.fillText(text, x, y);
    };
    return GameView;
}());
exports.GameView = GameView;
