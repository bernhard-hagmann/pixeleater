import {getNumberString} from "../Common";

export class GameView {

    canvas: any;
    context: any;
    fontSet: string = "georgia, 'times new roman', serif";
    monoSpaceFontSet: string = "'courier new', courier, arial, sans-serif";

    constructor(canvas) {
        this.canvas = canvas;
        this.context = this.canvas.getContext("2d");
        this.context.strokeStyle = "#fff";
    }

    renderIntro() {
        const logo = this.loadImage("logo.png");
        this.context.drawImage(logo, this.getMidX() - 300, this.getMidY() - 200);

        this.context.textAlign = "left";
        this.context.font = "4em " + this.fontSet;
        this.context.fillStyle = "black";
        this.printText("Pixel Eater", this.getMidX() - 50, this.getMidY() - 100);
        this.context.font = "1em " + this.fontSet;
        this.printText("v1.6", this.getMidX() + 200, this.getMidY() - 75);

        this.context.font = "1em " + this.fontSet;
        let x = this.getMidX() - 300;
        this.printText(
            "Become the biggest pixel-mob ever to rule the world (well, at least the canvas)!",
            x, this.getMidY());
        this.printText(
            "Use the arrow keys to move your blue pulsing pixels.",
            x, this.getMidY() + 30);
        this.printText(
            "Eat smaller pixel-mobs by tackling them and avoid bigger pixel-aggregations.",
            x, this.getMidY() + 60);

        this.renderNewGameKeyInfo(this.getMidX() - 250, this.getMidY() + 130);
    }

    renderGameEnd(player, maxPixelsEaten) {
        this.context.textAlign = "center";
        this.context.font = "3em " + this.fontSet;

        this.context.fillStyle = "red";
        this.printText(
            "Game over!",
            this.getMidX(), this.getMidY() - 30);

        this.context.fillStyle = "black";
        this.printText(
            "Pixels eaten: " + getNumberString(player.pixelsEaten),
            this.getMidX(), this.getMidY() + 30);

        this.context.font = "1em " + this.fontSet;
        this.printText(
            "Pixels eaten (best): " + getNumberString(maxPixelsEaten),
            this.getMidX(), this.getMidY() + 50);

        this.renderNewGameKeyInfo(this.getMidX(), this.getMidY() + 120);
    }

    renderGame(gameData) {
        for (let i = 0; i < gameData.enemies.length; i++) {
            const enemy = gameData.enemies[i];
            this.renderObject(enemy);
        }
        this.renderObject(gameData.player);
        this.renderGameInfo(gameData);
    }

    renderObject(obj) {
        this.context.fillStyle = obj.color;
        this.context.fillRect(obj.x, obj.y, obj.size, obj.size);

        this.context.strokeRect(obj.x - 1, obj.y - 1, obj.size + 1, obj.size + 1);
    }

    renderGameInfo(gameData) {
        this.context.textAlign = "left";
        this.context.font = "13px " + this.monoSpaceFontSet;
        this.context.fillStyle = "black";

        this.printText(
            "Zoom factor: " + gameData.zoomFactor,
            1, this.getCanvasTextBottom() - 14);

        const pixelsEaten = getNumberString(gameData.player.pixelsEaten);
        this.printText(
            "Pixels eaten: " + pixelsEaten,
            1, this.getCanvasTextBottom());
    }

    renderPauseOverlay() {
        this.context.textAlign = "center";
        this.context.font = "80px " + this.fontSet;

        this.printText(
            "PAUSE",
            this.getMidX(), this.getMidY());
    }

    renderInputState(inputState) {
        this.context.textAlign = "right";
        this.context.font = "13px " + this.monoSpaceFontSet;

        let stateString = "";
        if (inputState.left) {
            stateString += " < ";
        }
        if (inputState.up) {
            stateString += " ^ ";
        }
        if (inputState.down) {
            stateString += " v ";
        }

        if (inputState.right) {
            stateString += " > ";
        }
        if (inputState.n) {
            stateString += " n ";
        }
        if (inputState.p) {
            stateString += " p ";
        }

        this.printText(
            stateString,
            this.canvas.width - 1, this.getCanvasTextBottom());
    }

    getCanvasTextBottom() {
        return this.canvas.height - 3;
    }

    renderNewGameKeyInfo(x, y) {
        this.context.fillStyle = "black";
        this.context.font = "2em " + this.fontSet;
        this.printText(
            "Press 'N' to start a new game.",
            x, y);
    }

    clearCanvas() {
        this.context.fillStyle = "rgb(255,255,255)";
        this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
    }

    loadImage(name) {
        const img = new Image();
        img.src = "img/" + name;

        return img;
    }

    getMidX() {
        return this.canvas.width / 2;
    }

    getMidY() {
        return this.canvas.height / 2;
    }

    printText(text, x, y) {
        this.context.strokeText(text, x, y);
        this.context.fillText(text, x, y);
    }

}
