# PixelEater
PixelEater is a simple arcade game, where the user controls an entity that "eats" other entities to become bigger.

## ToDos and Ideas
* Use QuadTree to improve performance?
* Keep game objects centered while resizing
* Freeze movements while resizing
* Big objects move slower than small objects
* Gravity based on mass/size
* Resilience based on color?
* Collect all colors?
* Change own color depending on collected colors
* Sounds “gameover”, “nomnomnom”
* Animate “eating” pixels
* In-Game highscore board
* Rectangle game objects
* Control movement via touch swipes
* Scale game field size with browser window (don't increase field but scale objects)
* Enemies eat each other
* Fancy effects: Player leaves sparkles on his way (that fade after time)

## Changelog

### 2018-04-22
* Cleaned and merged sources
* Removed clunky buttons for touch input
* Game uses all available window space
* Made resizing faster
* Enabled WASD controls

### 2014-05-26
* Fixed missing logo in intro.
* Fixed pause exploit
* Smooth player control (dynamic vertical/horizontal speeds)
* Enemy color depending on size (eatable/non-eatable)
* Full window mode

### 2013-08-28
* When the player hits a certain size the view will be zoomed out
* The zoom-out is visualized by inverted canvas colors (until the next zoom-out)
* Maximum pixels per game are displayed

### 2012-10-09
* The game ends when a hostile pixel-aggregation eats the player
* The score equals the number of pixels eaten
* Pause button

### 2012-08-26
* The game can be played on smartphones
* The game can be played on low-resolution settings

### 2012-08-19
* Initial game
* The player object is a blue rectangle
* The player can move the player object using the cursor buttons
* AI objects are red rectangles
* AI objects have random width and height (within limits)
* AI objects move around autonomously
* When the player object hits a smaller AI object the player object becomes bigger
* When the player object hits a bigger AI object the game is over
* Every “eaten” AI object increases the game score
* The time required to win the game effects the game score
